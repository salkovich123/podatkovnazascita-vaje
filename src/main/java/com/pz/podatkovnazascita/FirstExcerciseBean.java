/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.podatkovnazascita;

import com.pz.podatkovnazascita.autokey.AutoKey;
import com.pz.podatkovnazascita.baconian.BaconianAlphabet;
import com.pz.podatkovnazascita.baconian.Letter;
import com.pz.podatkovnazascita.ejb.IOController;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.primefaces.event.FileUploadEvent;
import org.xml.sax.InputSource;

/**
 *
 * @author Marcelino
 */
@Named(value = "firstExcerciseBean")
@ViewScoped
public class FirstExcerciseBean implements Serializable {

    @EJB
    IOController ioc;
    @EJB
    PlayFairBean playfair;
    @EJB
    BaconianAlphabet bacon;
    @EJB
    AutoKey autokey;
    private boolean first;
    private boolean second;
    private boolean third;

    //first technique visibiity booleans
    private boolean thirdMode;
    private boolean thirdUpload;
    private boolean thirdUploadDecryption;
    private boolean thirdPreview;
    private boolean thirdTelemetry;
    private boolean secondMode;
    private boolean secondUpload;
    private boolean secondUploadDecryption;
    private boolean secondPreview;
    private boolean secondTelemetry;
    private boolean firstMode;
    private String extendedKey;
    private boolean firstUpload;
    private boolean firstUploadDecryption;
    private boolean firstPreview;
    private boolean firstTelemetry;
    private String keyword;
    private Date start;
    private Date end;
    private long timeBetween;
    private String upload_path;

    String preview;
    String preview_decrypt;
    //true if we cipher, false if we de-cipher
    private boolean mode;

    @PostConstruct
    public void init() {
        try {
            Letter l = autokey.encrypt1(ioc.readFile("/Users/Marcelino/Desktop/PZ/temp.txt"), "zimajelepa");
            System.out.println("EQUAL CHAR: "+ l.getText().length() + " " +l.getKey().length());
            autokey.decipher( l.getText(),l.getKey() );
        } catch (IOException ex) {
            Logger.getLogger(FirstExcerciseBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        keyword = "";
        preview = "";
        firstTelemetry = false;
        thirdTelemetry = false;
        first = false;
        second = false;
        third = false;
        mode = false;
        firstUploadDecryption = false;
        firstMode = true;
        secondMode = true;
        thirdMode = true;
        firstUpload = false;
        firstPreview = false;
        upload_path="";
        extendedKey = "";
        char ch = 154;
        System.out.println("Š :    " + (char) ch);
    }

    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
    //upload file for decryption
public void uploadDecryption() throws IOException{
       java.io.InputStream is = null;
       if(firstUpload){
        try {
            firstMode = false;
            firstUpload = false;
            firstPreview = true;
            System.out.println("Upload_ path : " + upload_path);
            is = new FileInputStream(new File(upload_path));
            
    ioc.uploadFile(is,"/Users/Marcelino/Desktop/PZ/decryptTmp.txt" );
    
                preview =""+ (String)ioc.readFile("/Users/Marcelino/Desktop/PZ/decryptTmp.txt");
                preview_decrypt = asciiToHex(preview);
                
                System.out.println("Preview is:   " + preview);
        
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FirstExcerciseBean.class.getName()).log(Level.SEVERE, null, ex);
        }
       }else if(thirdUpload){
           try {
            thirdMode = false;
            thirdUpload = false;
            thirdPreview = true;
    
                preview =(String)ioc.readFile(upload_path);
                preview_decrypt = asciiToHex(preview);
             
                System.out.println("Preview is:   " + preview);
        
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FirstExcerciseBean.class.getName()).log(Level.SEVERE, null, ex);
        }
       }
     
}
private static String asciiToHex(String asciiValue)
{
    char[] chars = asciiValue.toCharArray();
    StringBuilder hex = new StringBuilder();
    for (int i = 0; i < chars.length; i++)
    {
        hex.append(Integer.toHexString((int) chars[i]));
    }
    return hex.toString();
}
    public void handleFileUpload(FileUploadEvent evt) {
        if(first){
        try {
           
            ioc.CharsUploadPlayFair(evt.getFile().getInputstream(), "/Users/Marcelino/Desktop/PZ/temp.txt");
            preview = ioc.readFile("/Users/Marcelino/Desktop/PZ/temp.txt");
            
//System.out.println("output:"  + preview);
            firstMode = false;
            firstUpload = false;
            firstPreview = true;
        } catch (IOException ex) {
            Logger.getLogger(FirstExcerciseBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        }else if(second){
            if(mode){
            try {
           
            ioc.CharsUploadPlayFair(evt.getFile().getInputstream(), "/Users/Marcelino/Desktop/PZ/temp.txt");
            preview = ioc.readFile("/Users/Marcelino/Desktop/PZ/temp.txt");
            
//System.out.println("output:"  + preview);
            secondMode = false;
            secondUpload = false;
            secondPreview = true;
        } catch (IOException ex) {
            Logger.getLogger(FirstExcerciseBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        }else{
                try {
           
            ioc.CharsUploadPlayFair(evt.getFile().getInputstream(), "/Users/Marcelino/Desktop/PZ/bacon_ciphered.txt");
            preview = ioc.readFile("/Users/Marcelino/Desktop/PZ/bacon_ciphered.txt");
            
//System.out.println("output:"  + preview);
            secondMode = false;
            secondUpload = false;
            secondPreview = true;
        } catch (IOException ex) {
            Logger.getLogger(FirstExcerciseBean.class.getName()).log(Level.SEVERE, null, ex);
        }
            }
        }
        if(third){
             if(mode){
            try {
           
            ioc.CharsUploadPlayFair(evt.getFile().getInputstream(), "/Users/Marcelino/Desktop/PZ/temp.txt");
            preview = ioc.readFile("/Users/Marcelino/Desktop/PZ/temp.txt");
            
//System.out.println("output:"  + preview);
            thirdMode = false;
            thirdUpload = false;
            thirdPreview = true;
        } catch (IOException ex) {
            Logger.getLogger(FirstExcerciseBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        }else{
                try {
           
            ioc.CharsUploadPlayFair(evt.getFile().getInputstream(), "/Users/Marcelino/Desktop/PZ/autokey_ciphered.txt");
            preview = ioc.readFile("/Users/Marcelino/Desktop/PZ/autokey_ciphered.txt");
            
//System.out.println("output:"  + preview);
            thirdMode = false;
            thirdUpload = false;
            thirdPreview = true;
        } catch (IOException ex) {
            Logger.getLogger(FirstExcerciseBean.class.getName()).log(Level.SEVERE, null, ex);
        }
            }
        }
    }
    

    public void insertMode() {
        if(first){
        firstMode = false;
        System.out.println("mode is:    " + mode);
        firstUpload = true;
        }else if(second){
            secondMode = false;
            System.out.println("mode is:    " + mode);
            secondUpload = true;
        }else if(third){
            thirdMode = false;
            System.out.println("mode is:    " + mode);
            thirdUpload = true;
        }
      
        
    }

    public void viewFirst() {
        first = true;
        second = false;
        third = false;
    }

    public void viewSecond() {
        first = false;
        second = true;
        third = false;
    }

    public void viewThird() {
        first = false;
        second = false;
        third = true;
    }
    public void bacon(){
        DateTime start1 = null;
        DateTime end1 = null;
       if(mode){
           start1 = new DateTime();
           bacon.getAlphabet();
        bacon.encryptMessage();
        bacon.cipherEncrypted();
        end1 = new DateTime();
       }else{
         start1 = new DateTime();
         bacon.getAlphabet();
        bacon.cipherDecrypted();
        bacon.decryptMessage();
        end1= new DateTime();
       }
        secondTelemetry = true;
        
        timeBetween = end1.getMillis()-start1.getMillis();
        start = start1.toDate();
        end = end1.toDate();
       
    }

    public void playfair() {
        DateTime start1 =null;
        DateTime end1 = null;
        System.out.println("keyword is:   "  + keyword);
        if (mode) {
            start1 = new DateTime();
            playfair.startEncryption(keyword);
            end1 = new DateTime();
        } else {
            start1 = new DateTime();
            playfair.startDecription(keyword);
            end1 = new DateTime();
        }
        firstTelemetry = true;
        
        timeBetween = end1.getMillis()-start1.getMillis();
        start = start1.toDate();
        end = end1.toDate();
    }
public void autokey() {
     DateTime start1 =null;
        DateTime end1 = null;
    if(mode){
  
            start1 = new DateTime();
            Letter l = null;
         try {
             l = autokey.encrypt1(ioc.readFile("/Users/Marcelino/Desktop/PZ/temp.txt"), keyword);
         } catch (IOException ex) {
             Logger.getLogger(FirstExcerciseBean.class.getName()).log(Level.SEVERE, null, ex);
         }
            
            end1 = new DateTime();
            this.preview = l.getText();
            ioc.saveEncoded(l.getText(),"/Users/Marcelino/Desktop/PZ/autokey_ciphered.txt");
            extendedKey = l.getKey();
            System.out.println("KEY:   " +l.getKey());
      
               
       
    }else{
       
            start1 = new DateTime();
            preview = autokey.decipher(preview, keyword);
            
            end1 = new DateTime();
       
    }
              thirdTelemetry = true;
        
        timeBetween = end1.getMillis()-start1.getMillis();
        start = start1.toDate();
        end = end1.toDate();
        System.out.println("Končal z date" + thirdTelemetry +" START: "+ start +" end: "+ end);
        
   
}
    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }

    public boolean isSecond() {
        return second;
    }

    public void setSecond(boolean second) {
        this.second = second;
    }

    public boolean isThird() {
        return third;
    }

    public void setThird(boolean third) {
        this.third = third;
    }

    public boolean isMode() {
        return mode;
    }

    public void setMode(boolean mode) {
        this.mode = mode;
    }

    public boolean isFirstMode() {
        return firstMode;
    }

    public void setFirstMode(boolean firstMode) {
        this.firstMode = firstMode;
    }

    public boolean isFirstUpload() {
        return firstUpload;
    }

    public void setFirstUpload(boolean firstUpload) {
        this.firstUpload = firstUpload;
    }

    public boolean isFirstPreview() {
        return firstPreview;
    }

    public void setFirstPreview(boolean firstPreview) {
        this.firstPreview = firstPreview;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getKeyword() {
        return keyword;
    }

    public boolean isFirstTelemetry() {
        return firstTelemetry;
    }

    public void setFirstTelemetry(boolean firstTelemetry) {
        this.firstTelemetry = firstTelemetry;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public boolean isFirstUploadDecryption() {
        return firstUploadDecryption;
    }

    public void setFirstUploadDecryption(boolean firstUploadDecryption) {
        this.firstUploadDecryption = firstUploadDecryption;
    }

    public String getUpload_path() {
        return upload_path;
    }

    public void setUpload_path(String upload_path) {
        this.upload_path = upload_path;
    }

    public String getPreview_decrypt() {
        return preview_decrypt;
    }

    public void setPreview_decrypt(String preview_decrypt) {
        this.preview_decrypt = preview_decrypt;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public long getTimeBetween() {
        return timeBetween;
    }

    public void setTimeBetween(long timeBetween) {
        this.timeBetween = timeBetween;
    }

    public boolean isSecondMode() {
        return secondMode;
    }

    public void setSecondMode(boolean secondMode) {
        this.secondMode = secondMode;
    }

    public AutoKey getAutokey() {
        return autokey;
    }

    public void setAutokey(AutoKey autokey) {
        this.autokey = autokey;
    }

    public boolean isSecondUpload() {
        return secondUpload;
    }

    public void setSecondUpload(boolean secondUpload) {
        this.secondUpload = secondUpload;
    }

    public boolean isSecondUploadDecryption() {
        return secondUploadDecryption;
    }

    public void setSecondUploadDecryption(boolean secondUploadDecryption) {
        this.secondUploadDecryption = secondUploadDecryption;
    }

    public boolean isSecondPreview() {
        return secondPreview;
    }

    public void setSecondPreview(boolean secondPreview) {
        this.secondPreview = secondPreview;
    }

    public boolean isSecondTelemetry() {
        return secondTelemetry;
    }

    public void setSecondTelemetry(boolean secondTelemetry) {
        this.secondTelemetry = secondTelemetry;
    }

    public boolean isThirdMode() {
        return thirdMode;
    }

    public void setThirdMode(boolean thirdMode) {
        this.thirdMode = thirdMode;
    }

    public boolean isThirdUpload() {
        return thirdUpload;
    }

    public void setThirdUpload(boolean thirdUpload) {
        this.thirdUpload = thirdUpload;
    }

    public boolean isThirdUploadDecryption() {
        return thirdUploadDecryption;
    }

    public void setThirdUploadDecryption(boolean thirdUploadDecryption) {
        this.thirdUploadDecryption = thirdUploadDecryption;
    }

    public boolean isThirdPreview() {
        return thirdPreview;
    }

    public void setThirdPreview(boolean thirdPreview) {
        this.thirdPreview = thirdPreview;
    }

    public boolean isThirdTelemetry() {
        return thirdTelemetry;
    }

    public void setThirdTelemetry(boolean thirdTelemetry) {
        this.thirdTelemetry = thirdTelemetry;
    }

    public String getExtendedKey() {
        return extendedKey;
    }

    public void setExtendedKey(String extendedKey) {
        this.extendedKey = extendedKey;
    }

}
