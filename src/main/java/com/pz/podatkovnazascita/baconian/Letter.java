/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.podatkovnazascita.baconian;

/**
 *
 * @author Marcelino
 */
public class Letter {
    String text;
    String key;
   
    
    
    public  Letter(String text,String key){
        this.text = text;
        this.key=key;
    }

    public String getText() {
        return text;
    }

    public void setLetter(String text) {
        this.text = text;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
    
    
}
