/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.podatkovnazascita;

import com.pz.podatkovnazascita.ejb.IOController;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Marcelino
 */
@Stateless
public class PlayFairBean implements Serializable {

    @EJB
    IOController ioc;

    private int[][] files;
   

    @PostConstruct
    public void init() {
        files = new int[16][16];
        
    }

    public void prepareKey(String keyword) {
        int keyword_size_i = keyword.length() / 16;
        int keyword_size_j = keyword.length() % 16;

        System.out.println("Keyword size i: " + keyword_size_i + " Keyword size j: " + keyword_size_j + " keyword " + keyword);

        String key = stripDuplicates(keyword);
        System.out.println("Key:  " + key);
        populateTable(key);
        


             
         //testing purposes
        
         for (int i = 0; i < 16; i++) {
             String out = "";
         for (int j = 0; j < 16; j++) {
         out = out + (char) files[i][j];
         }
         System.out.println(out);

         }
         
    }

    //remove duplicate chars in keyword
    public String stripDuplicates(String keyword) {
        StringBuilder noDupes = new StringBuilder();
        for (int i = 0; i < keyword.length(); i++) {
            String si = keyword.substring(i, i + 1);

            if (noDupes.indexOf(si) == -1) {
                noDupes.append(si);
            }
        }
        return noDupes.toString();
    }

//populate cipher table with key and other ascii values
    public void populateTable(String keyword) {
        int index = 0;
        List<Integer> temp = new ArrayList();
        //add keyword to array
        for (int i = 0; i < keyword.length(); i++) {
            temp.add((int) keyword.charAt(i));

        }
        //add other ascii to arrayList
        for (int i = 0; i < 256; i++) {
            if(i ==0){
                temp.add("š".codePointAt(index));
            }
            if(i ==1){
                temp.add("Š".codePointAt(index));
            }
            if(i ==2){
                temp.add("ž".codePointAt(index));
            }
            if(i ==3){
                temp.add("Ž".codePointAt(index));
            }if(i==127){
                
            }
            if(i==126){
                
            }
            if(i==128){
                
            }else{
                
            temp.add(i+28);
            }
            
        }
        // remove duplicates in arrayList
        for (int i = keyword.length(); i < temp.size(); i++) {
            for (int j = 0; j < keyword.length(); j++) {
                if (temp.get(i) == keyword.charAt(j)) {
                    temp.remove(i);
                }
            }
        }
        //transform arrayList to array
        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 16; j++) {
                files[i][j] = temp.get(index);
                index++;
            }
        }

    }

    public String checkPlainTextDuplicates() {
        try {
            String plainText = ioc.readFile("/Users/Marcelino/Desktop/PZ/temp.txt");
            String tmp = "";
            System.out.println("PlainText.length" + plainText.length());
 System.out.println("length is:  " + plainText.length());
            if (plainText.length() % 2 != 0) {
                System.out.println("Add X!!");
                plainText = plainText + "X";
            }
            for (int i = 0; i < plainText.length(); i = i + 2) {
                if (plainText.charAt(i) == plainText.charAt(i + 1)) {
                    System.out.println("Found a duplicate:   " + plainText.charAt(i) + "  " + plainText.charAt(i + 1) + "iteration: " + i);
                    tmp = tmp + plainText.charAt(i) + "X" + plainText.charAt(i + 1);
                } else {
                    tmp = tmp + plainText.charAt(i) + plainText.charAt(i + 1);
                }
            }
            //Add x at the end if  length of text is not dividable by 2
           
            return tmp;
        } catch (IOException ex) {
            Logger.getLogger(PlayFairBean.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }
//divide text in pairs of two
    public String[] divide2Pairs(String plainText) {
        int size = plainText.length();
        System.out.println("Size is:    " + size);
        String[] pairs = new String[size / 2];
        int pairCounter = 0;

        for (int i = 0; i < (size / 2); i++) {
            pairs[i] = plainText.substring(pairCounter, pairCounter + 2);
            System.out.println("Pairs     " + plainText.substring(pairCounter, pairCounter + 2) + "iteration: " + pairCounter);
            pairCounter = pairCounter + 2;
        }
        return pairs;
    }

    //Get dimension of  letter in our matrix

    public int[] getDimensions(char letter) {
        int[] position = new int[2];
        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 16; j++) {
                if (files[i][j] == letter) {
                    position[0] = i;
                    position[1] = j;
                }
            }
        }
        return position;
    }

    //encrypt plaintext by playfair rules

    public void encrypt(String keyword) {
         prepareKey(keyword);
        System.out.println("Encription started");
        char one;
        char two;
        int[] letter1 = new int[2];
        int[] letter2 = new int[2];
        String plainText = checkPlainTextDuplicates();
        String result ="";
        String[] pairs = divide2Pairs(plainText);
        for (int a = 0; a < pairs.length; a++) {
            one = pairs[a].charAt(0);
            two = pairs[a].charAt(1);
            letter1 = getDimensions(one);
            letter2 = getDimensions(two);
            //if two letters are in same row
            if (letter1[0] == letter2[0]) {
                //if letter 1 column is last we ned to shift it at start
                if (letter1[1] < 15) {
                    letter1[1]++;
                } else {
                    letter1[1] = 0;
                }
                //if letter 2 column is last we have to shift it at start
                if (letter2[1] < 15) {
                    letter2[1]++;
                } else {
                    letter2[1] = 0;
                }
            } //if two letters are in same column
            else if (letter1[1] == letter2[1]) {
                if(letter1[0]<15){
                    letter1[0]++;
                }else{
                    letter1[0]=0;
                }
                if(letter2[0]<15){
                    letter2[0]++;
                }else{
                   letter2[0] = 0;
                }
            } //if two letters are in different rows and columns
            else {
                int column_y = letter1[1];
                    letter1[1] = letter2[1];
                    letter2[1] = column_y;
            }
            result = result +(char) files[letter1[0]][letter1[1]] + (char) files[letter2[0]][letter2[1]];
            
        }
        System.out.println("Encrypted text is:     " + result);
        ioc.saveEncoded((String)result,"/Users/Marcelino/Desktop/PZ/result.txt");
        
    }

   public void startEncryption(String keyword){
      
       encrypt(keyword);
       System.out.println("Done encrypting :)");
       
   }
   public void startDecription(String keyword){
       
       
            decrypt(keyword);
       
       System.out.println("Done decripting");
   }
    //decription of text in file
    public void decrypt(String keyword) 
    {
        try {
            prepareKey(keyword);
            System.out.println("Decription started");
            
            String text = ioc.readFile("/Users/Marcelino/Desktop/PZ/result.txt");
            char one;
            char two;
            int[] letter1 = new int[2];
            int[] letter2 = new int[2];
            String result ="";
            String[] pairs = divide2Pairs(text);
            for (int a = 0; a < pairs.length; a++) {
                one = pairs[a].charAt(0);
                two = pairs[a].charAt(1);
                letter1 = getDimensions(one);
                letter2 = getDimensions(two);
                //if two letters are in same row
                if (letter1[0] == letter2[0]) {
                    //if letter 1 column is first we neEd to shift it at last
                    if (letter1[1]==0) {
                        letter1[1]=15;
                    } else {
                        letter1[1]--;
                    }
                    //if letter 2 column is last we have to shift it at start
                    if (letter2[1]== 0) {
                        letter2[1]=15;
                    } else {
                        letter2[1]--;
                    }
                } //if two letters are in same column
                else if (letter1[1] == letter2[1]) {
                    if(letter1[0]==0){
                        letter1[0]=15;
                    }else{
                        letter1[0]--;
                    }
                    if(letter2[0]==0){
                        letter2[0]=15;
                    }else{
                        letter2[0]--;
                    }
                } //if two letters are in different rows and columns
                else {
                    int column_y = letter1[1];
                    letter1[1] = letter2[1];
                    letter2[1] = column_y;
                   
                }
                result = result +(char) files[letter1[0]][letter1[1]] + (char) files[letter2[0]][letter2[1]];
                result = result.replaceAll("X", "");
            }
            System.out.println("Decrypted text is:     " + result);
            ioc.saveEncoded(result,"/Users/Marcelino/Desktop/PZ/decrypted.txt");
        } catch (IOException ex) {
            System.out.println("error decripting");
            Logger.getLogger(PlayFairBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        
    }

    
}
