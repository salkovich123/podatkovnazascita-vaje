/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.podatkovnazascita.baconian;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.pz.podatkovnazascita.ejb.IOController;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Marcelino
 */
@Stateless
public class BaconianAlphabet {
    @EJB
     IOController ioc; 
   HashBiMap<String,String> letters1 =null;
   HashBiMap<String,String> mashes =null;
   String encrypted;
   String ciphered;
   String uncyphered;
   BiMap<String,String> mashesInverse;
   BiMap<String,String> letters1Inverse;
    private String decrypted;
    public void getAlphabet(){
        
        
            /*  letters1 = new ArrayList();
    
       
       for(int i = 32;i<256;i++){
           
           char ch = (char)i;
           
           letters1.add(new Letter(""+ch,Integer.toString(i+68, 2)));
       }
       letters1.add(new Letter("č", Integer.toString(324,2)));
       letters1.add(new Letter("Č", Integer.toString(325, 2))); 
       letters1.add(new Letter("ž", Integer.toString(326, 2)));
       letters1.add(new Letter("Ž", Integer.toString(327, 2)));
       letters1.add(new Letter("š", Integer.toString(328, 2)));
       letters1.add(new Letter("Š", Integer.toString(329, 2)));
       
       
       
       
       
       for(int j = 0;j<letters1.size();j++){
         if(letters1.get(j).binary.length()!=9){
            if(letters1.get(j).binary.length()==7){
                String tmp ="00"+letters1.get(j).getBinary();
                letters1.get(j).setBinary(tmp);
            }
            if(letters1.get(j).binary.length()==8){
                 String tmp ="0"+letters1.get(j).getBinary();
                letters1.get(j).setBinary(tmp);
       
       
            }
         }
       }
       
       
      
     */
    letters1= HashBiMap.create();
    mashes = HashBiMap.create();
       
       for(int i = 32;i<256;i++){
           
           char ch = (char)i;
           String binary="";
           if(Integer.toString(i+68, 2).length()==7){
               binary = "00"+Integer.toString(i+68, 2);
           }
           if(Integer.toString(i+68, 2).length()==8){
               binary = "0"+Integer.toString(i+68, 2);
           }else{
               binary = Integer.toString(i+68, 2);
           }
           letters1.forcePut(""+ch,binary);
       }
       letters1.forcePut("č", Integer.toString(324,2));
       letters1.forcePut("Č", Integer.toString(325, 2)); 
       letters1.forcePut("ž", Integer.toString(326, 2));
       letters1.forcePut("Ž", Integer.toString(327, 2));
       letters1.forcePut("š", Integer.toString(328, 2));
       letters1.forcePut("Š", Integer.toString(329, 2));
       letters1.forcePut("Ü", Integer.toString(330, 2));
       letters1.forcePut("Ö", Integer.toString(331, 2));
       letters1.forcePut("ü", Integer.toString(332, 2));
       letters1.forcePut("ö", Integer.toString(333, 2));
       letters1.forcePut("ó", Integer.toString(334, 2));
       letters1.forcePut("	", Integer.toString(335, 2));
       
       
       
       
     mashes.forcePut("1", "!");
     mashes.forcePut("0", "-");
     mashes.forcePut(" ", " ");
     
      mashesInverse = mashes.inverse();
      letters1Inverse = letters1.inverse();
      
       
       
    }
    public void encryptMessage(){
        encrypted = "";
        try {
            String message = ioc.readFile("/Users/Marcelino/Desktop/PZ/temp.txt");
            for(int i = 0;i< message.length();i++){
                encrypted = encrypted +  letters1.get(""+message.charAt(i)) + " ";
                
            }
            
        } catch (IOException ex) {
            Logger.getLogger(BaconianAlphabet.class.getName()).log(Level.SEVERE, null, ex);
        }
 
    }
    public void decryptMessage(){
        decrypted = "";
        
            String[] words = uncyphered.split(" ");
        for(int i = 0;i<words.length;i++){
            if(words[i]!=null){
            decrypted = decrypted + letters1Inverse.get(words[i]);
            }
            else{
                decrypted = decrypted + " ";
            }
        }
        ioc.saveEncoded(decrypted, "/Users/Marcelino/Desktop/PZ/originl_bacon.txt");
    }
    //set - and ! instead of 0 and 1
    public void cipherEncrypted()
    {
        ciphered="";
     for(int i = 0; i<encrypted.length();i++){
        
            ciphered = ciphered + mashes.get("" + encrypted.charAt(i));
        
       
    }
    ioc.saveEncoded(ciphered, "/Users/Marcelino/Desktop/PZ/bacon_ciphered.txt");
    }
    public void cipherDecrypted(){
        uncyphered = "";
        
        try {
            String fileContent = ioc.readFile("/Users/Marcelino/Desktop/PZ/bacon_ciphered.txt");
            for(int i=0;i<fileContent.length();i++){
                uncyphered = uncyphered + mashesInverse.get(""+fileContent.charAt(i));
            }
        } catch (IOException ex) {
            Logger.getLogger(BaconianAlphabet.class.getName()).log(Level.SEVERE, null, ex);
        }
    System.out.println("uncyphered " + uncyphered);
    }
    
}
