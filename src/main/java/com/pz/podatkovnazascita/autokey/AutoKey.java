/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pz.podatkovnazascita.autokey;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.pz.podatkovnazascita.baconian.Letter;
import com.pz.podatkovnazascita.ejb.IOController;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Marcelino
 */
@Stateless
public class AutoKey implements Serializable {

    @EJB
    IOController ioc;

    
 
 
 
 public Letter encrypt1(String plaintext, String key){
     String keyword= key + plaintext.substring(0, (plaintext.length()-key.length()));
     StringBuilder result = new StringBuilder();
     for(int i=0;i<plaintext.length();i++){
         char encyphered   =0;
         if(plaintext.charAt(i)+getShift(keyword, i)>500){
             encyphered = (char)((plaintext.charAt(i)+getShift(keyword, i))-500);
         }else{
             encyphered = (char)(plaintext.charAt(i)+getShift(keyword, i));
         }
         result.append(encyphered);
     }
     System.out.println("Encyphered: "  + result.toString());
     return new Letter(result.toString(),keyword);
 }
 private int getShift(String key,int i){
     return ((int) key.charAt(i%key.length()));
 }
 public String decipher(String plainText, String key){
     StringBuilder decyphered = new StringBuilder();
     for(int i = 0;i<plainText.length();i++){
         char temp = 0;
         if((plainText.charAt(i)-getShift(key, i))<0){
             temp = (char)(plainText.charAt(i) -getShift(key, i)+500);
         }else{
             temp = (char)(plainText.charAt(i)-getShift(key, i));
         }
         decyphered.append(temp);
     }
     System.out.println("Decyphered:  "+decyphered);
     ioc.saveEncoded(decyphered.toString(), "/Users/Marcelino/Desktop/PZ/autokey_deciphered.txt");
     return decyphered.toString();
 }
  
  

}
